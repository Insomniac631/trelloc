﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trello.Models.Comments;
using Trello.Models.Members;

namespace Trello.Models.Cards
{
    public class CardService : TrelloAPIBase
    {
        public CardService(){}

        public async Task<IEnumerable<Card>> ForBoard(string boardId)
        {
             var list = GetRequest<List<Card>>("/boards/{0}/cards", boardId);
             List<Card> l = await list;
             return l;
        }

        public async Task<IEnumerable<Card>> ForList(string listId)
        {
            var list = GetRequest<List<Card>>("/lists/{0}/cards", listId);
            List<Card> l = await list;
            return l;
        }

        public async Task<Card> Single(string cardId)
        {
            var list = GetRequest<Card>("/cards/{0}", cardId);
            Card l = await list;
            return l;
        }

        //TODO: check how we put data and what data is returned
        //public async Task<Card> Update(Card card)
        //{
        //    var card = PutRequestWithJsonNet<Card, Card>(card, "/cards/{0}", card.Id);
        //}

        //public Card Create(Card card)
        //{
        //    return PostRequest<Card, Card>(card, "/cards");
        //}

        public async Task<List<Comment>> AddComment(string cardId, string commentText)
        {
            var comment = PostRequest<List<Comment>, object>(new { text = commentText }, "/cards/{0}/actions/comments", cardId);
            List<Comment> c = await comment;
            return c;
        }

        public async Task<List<Member>> AddMember(string cardId, string memberId)
        {
            var member = PostRequest<List<Member>, object>(new { value = memberId }, "/cards/{0}/members", cardId);
            List<Member> m = await member;
            return m;
        }

        public void RemoveMember(string cardId, string memberId)
        {
            DeleteRequest("/cards/{0}/members/{1}", cardId, memberId);
        }
    }
}

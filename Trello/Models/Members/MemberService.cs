﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trello.Models.Boards;
using Trello.Models.Extensions;
using Trello.Models.Notifications;
using Trello.Models.Organizations;

namespace Trello.Models.Members
{
    public class MemberService : TrelloAPIBase
    {
        public async Task<Member> Single(string username)
        {
            var member = GetRequest<Member>("/members/{0}", username);
            Member m = await member;
            return m;
        }

        public async Task<IEnumerable<Member>> ForCard(string cardId)
        {
            var list = GetRequest<List<Member>>("/cards/{0}/members", cardId);
            List<Member> l = await list;
            return l;
        }

        public async Task<IEnumerable<Member>> ForBoard(string boardId)
        {
            var list = GetRequest<List<Member>>("/boards/{0}/members", boardId);
            List<Member> l = await list;
            return l;
        }

        public async Task<string> GetMemberId() 
        {
            var id = GetRequest<Member>("members/me");
            Member Id = await id;
            System.Diagnostics.Debug.WriteLine(Id.Id);
            return Id.Id;
        }

        public async Task<Member> GetMember(string Id) 
        {
            var resp = GetRequest<Member>("members/{0}");
            Member member = await resp;
            return member;
        }

        public async Task<string> Bio(string username)
        {
            var str = GetRequest("/members/{0}/bio", username);
            string s = await str;
            return s;
        }

        public async Task<string> Fullname(string username)
        {
            var str = GetRequest<StringValue>("/members/{0}/fullname", username);
            StringValue s = await str;
            return s.Value;
        }

        public async Task<string> Gravatar(string username)
        {
            var str = GetRequest<StringValue>("/members/{0}/gravatar", username);
            StringValue s = await str;
            return s.Value;
        }

        public async Task<string> Username(string userId)
        {
            var str = GetRequest<StringValue>("/members/{0}/username", userId);
            StringValue s = await str;
            return s.Value;
        }

        public async Task<string> Url(string username)
        {
            var str = GetRequest<StringValue>("/members/{0}", username);
            StringValue s = await str;
            return s.Value;
        }

        public async Task<IEnumerable<Organization>> Organizations(string username)
        {
            var list = GetRequest<List<Organization>>("/members/{0}/organizations", username);
            List<Organization> l = await list;
            return l;
        }

        public async Task<IEnumerable<Member>> MembersInOrganizations(string username)
        {
            var list = GetRequest<List<Member>>("/members/{0}/organizations/members", username);
            List<Member> l = await list;
            return l;
        }

        public async Task<IEnumerable<Notification>> Notifications(string username)
        {
            //TODO: Check one more time if this needed
            //if (string.IsNullOrEmpty(AuthToken))
            //    throw new ChelloException("The AuthToken setting is null/empty but is required for notifications.");

            var list = GetRequest<List<Notification>>("/members/{0}/notifications", username);
            List<Notification> l = await list;
            return l;
        }

        public async Task<IEnumerable<Notification>> ReadNotifications(string username)
        {
        //    if (string.IsNullOrEmpty(AuthToken))
        //        throw new ChelloException("The AuthToken setting is null/empty but is required for read notifications.");

            var list = GetRequest<List<Notification>>("/members/{0}/notifications", username);
            List<Notification> l = await list;
            return l;
        }

        public async Task<IEnumerable<Notification>> UnreadNotifications(string username)
        {
            //if (string.IsNullOrEmpty(AuthToken))
            //    throw new ChelloException("The AuthToken setting is null/empty but is required for unread notifications.");

            var list = GetRequest<List<Notification>>("/members/{0}/notifications", username);
            List<Notification> l = await list;
            return l;
        }

        public async Task<IEnumerable<Board>> AllBoards(string username)
        {
            var list = GetRequest<List<Board>>("/members/{0}/boards", username);
            List<Board> l = await list;
            return l;
        }

        public async Task<IEnumerable<Board>> PublicBoards(string username)
        {
            var list = GetRequest<List<Board>>("/members/{0}/boards/public", username);
            List<Board> l = await list;
            return l;
        }

        #region NotImplemented
        //public IEnumerable<Board> PinnedBoards(string username)
        //{
        //    throw new NotImplementedException("Calls requiring authentication tokens haven't been implemented yet");

        //    //return GetRequest<List<Board>>("/members/{0}/boards/pinned", username);
        //}

        //public IEnumerable<Board> UnPinnedBoards(string username)
        //{
        //    throw new NotImplementedException("Calls requiring authentication tokens haven't been implemented yet");

        //    //return GetRequest<List<Board>>("/members/{0}/boards/unpinned", username);
        //}

        //public IEnumerable<Card> Cards(string username)
        //{
        //    throw new NotImplementedException("Cards by username hasn't been implemented yet");

        //    //return GetRequest<List<Card>>("/members/{0}/cards", username);
        //}
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trello.Models.Boards
{
    public class BoardService :TrelloAPIBase
    {

        public async Task<IEnumerable<Board>> ForUser(string username)
        {
            Task<List<Board>> getList = GetRequest<List<Board>>("/members/{0}/boards", username);
            List<Board> list = await getList;
            return list;
        }

        public async Task<IEnumerable<Board>> ForOrganization(string organizationName)
        {
            Task<List<Board>> getList = GetRequest<List<Board>>("organizations/{0}/boards", organizationName);
            List<Board> list = await getList;
            return list;
        }


        public async Task<Board> Single(string boardId)
        {
            Task<Board> getBoard = GetRequest<Board>("/boards/{0}", boardId);
            Board board = await getBoard;
            return board;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trello.Models.Boards;
using Trello.Models.Members;

namespace Trello.Models.Organizations
{
    public class OrganizationService : TrelloAPIBase
    {
        public async Task<Organization> Single(string organizationName)
        {
            var organ = GetRequest<Organization>("/organizations/{0}", organizationName);
            Organization o = await organ;
            return o;
        }

        // TODO: Some methods are blah.ForSomething, and others are something.AllBlahs()... Can we make this more consistent?
        public async Task<IEnumerable<Board>> AllBoards(string organizationName)
        {
            var list = GetRequest<List<Board>>("/organizations/{0}/boards", organizationName);
            List<Board> l = await list;
            return l;
        }

        public async Task<IEnumerable<Member>> AllMembers(string organizationName)
        {
            var list = GetRequest<List<Member>>("/organizations/{0}/members", organizationName);
            List<Member> l = await list;
            return l;
        }

        public async Task<string> Name(string organizationName)
        {
            var str = GetRequest("/organizations/{0}/name", organizationName);
            string s = await str;
            return s;
        }

        public async Task<string> DisplayName(string organizationName)
        {
            var str = GetRequest("/organizations/{0}/displayName", organizationName);
            string s = await str;
            return s;
        }

        public async Task<string> Description(string organizationName)
        {
            var str = GetRequest("/organizations/{0}/desc", organizationName);
            string s = await str;
            return s;
        }

        public async Task<string> Url(string organizationName)
        {
            var str = GetRequest("/organizations/{0}/url", organizationName);
            string s = await str;
            return s;
        }
    }
}

﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Trello.Models.Lists
{
    [DataContract]
    public class List
    {
        [JsonProperty("id")]
        [DataMember(Name = "id")]
        public string Id { get; set; }

        [JsonProperty("idboard")]
        [DataMember(Name = "idboard")]
        public string IdBoard { get; set; }

        [JsonProperty("name")]
        [DataMember(Name = "name")]
        public string Name { get; set; }
    }
}

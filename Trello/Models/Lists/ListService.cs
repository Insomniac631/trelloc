﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trello.Models.Lists
{
    public class ListService : TrelloAPIBase
    {
        public async Task<IEnumerable<List>> ForBoard(string boardId)
        {
            var list = GetRequest<List<List>>("/boards/{0}/lists", boardId);
            List<List> l = await list;
            return l;
        }

        public async Task<List> Single(string listId)
        {
            var list = GetRequest<List>("/lists/{0}", listId);
            List l = await list;
            return l;
        }
    }
}

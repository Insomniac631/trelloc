﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Trello.Models.Extensions
{
    public class StringValue : ITrelloEntity
    {
        [JsonProperty]
        [DataMember(Name = "_value")]
        public string Value { get; set; }
    }
}

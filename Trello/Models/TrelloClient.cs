﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trello.Models.Boards;
using Trello.Models.Cards;
using Trello.Models.Lists;
using Trello.Models.Members;
using Trello.Models.Organizations;

namespace Trello.Models
{
    public class TrelloClient
    {
        private static volatile TrelloClient instance;

        private static object syncRoot = new Object();
        public static TrelloClient Instance
        { 
            get 
            {
                if (instance == null)
                {
                    lock (syncRoot) 
                    {
                        if (instance == null)
                            instance = new TrelloClient();
                    }
                }
                return instance;
            }
        }

        private TrelloClient() 
        {
            this.Boards = new BoardService();
            this.Lists = new ListService();
            this.Cards = new CardService();
            //this.Comments = new CommentService();
            //this.CardUpdates = new CardUpdateService();
            this.Members = new MemberService();
            //this.Notifications = new NotificationService();
            this.Organizations = new OrganizationService();
            Init();
        }

        private async void Init() 
        {
            string r = await this.Members.GetMemberId();
            this.MainMember = await this.Members.GetMember(r);
            System.Diagnostics.Debug.WriteLine("Member Fullname : {0}", this.MainMember.FullName);
        }
        

        #region Properties

        public BoardService Boards { get; private set; }
        public ListService Lists { get; private set; }
        public CardService Cards { get; private set; }
        //public CommentService Comments { get; private set; }
        //public CardUpdateService CardUpdates { get; private set; }
        public MemberService Members { get; private set; }
        //public NotificationService Notifications { get; private set; }
        public OrganizationService Organizations { get; private set; }

        public Member MainMember { get; set; }

        #endregion

    }
}

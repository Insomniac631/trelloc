﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trello.Common;

namespace Trello.Models
{
    public class TrelloAPIBase
    {
        App app = (App.Current as App);

        protected TrelloAPIBase()
        {
            if (string.IsNullOrEmpty(app.OAuthToken) || string.IsNullOrEmpty(app.OAuthTokenSecret))
            {
                throw new ArgumentException("Authentification key required!");
            }
        }

        protected async Task<T> GetRequest<T>(string path, params string[] args) where T : new()
        {
            RestRequest request = new RestRequest(BuildUrl(path, args));

            var response = await app.Client.ExecuteAwaitableAsync<T>(request);
            return response.Data;
        }

        protected async Task<string> GetRequest(string path, params string[] args)
        {
            RestRequest request = new RestRequest(BuildUrl(path, args));

            System.Diagnostics.Debug.WriteLine(BuildUrl(path, args));

            var response = await app.Client.ExecuteTask(request);

            return response.Content;
        }

        protected async Task<TOutput> PutRequest<TOutput, TInput>(TInput obj, string path, params string[] args) where TOutput : new()
        {
            return await Request<TOutput, TInput>(Method.PUT, obj, path, args);
        }

        protected async Task<TOutput> PutRequestWithJsonNet<TOutput, TInput>(TInput obj, string path, params string[] args) where TOutput : new()
        {
            return await RequestWithLegacySerializer<TOutput, TInput>(Method.PUT, obj, path, args);
        }

        protected async Task<TOutput> PostRequest<TOutput, TInput>(TInput obj, string path, params string[] args) where TOutput : new()
        {
            return await Request<TOutput, TInput>(Method.POST, obj, path, args);
        }

        protected async void DeleteRequest(string path, params string[] args)
        {
            await Request<List<object>, object>(Method.DELETE, null, path, args);
        }

        private async Task<TOutput> Request<TOutput, TInput>(Method method, TInput obj, string path, params string[] args) where TOutput : new()
        {
            RestRequest request = new RestRequest(BuildUrl(path, args), method);

            request.RequestFormat = DataFormat.Json;
            request.AddBody(obj);

            var response = await app.Client.ExecuteAwaitableAsync<TOutput>(request);

            return response.Data;
        }

        /// <summary>
        /// Uses the old JSON.NET serializer so JSON.NET Json[] attributes are supported.
        /// </summary>
        private async Task<TOutput> RequestWithLegacySerializer<TOutput, TInput>(Method method, TInput obj, string path, params string[] args) where TOutput : new()
        {
            RestRequest request = new RestRequest(BuildUrl(path, args), method);
            request.JsonSerializer = new LegacyJsonSerializer();

            request.RequestFormat = DataFormat.Json;
            request.AddBody(obj);

            var response = await app.Client.ExecuteAwaitableAsync<TOutput>(request);

            return response.Data;
        }

        protected string BuildUrl(string path, params string[] args)
        {
            // Assume for now that no querystring is added
            path = string.Format(path, args);
            path = string.Format("{0}{1}key={2}", path, path.Contains("?") ? "&" : "?", app.ConsumerKey);

            if (!string.IsNullOrEmpty(app.OAuthToken))
                path = string.Format("{0}&token={1}", path, app.OAuthToken);

            return path;
        }

        protected static string BuildQueryString(object args)
        {
            string queryString = String.Empty;
            if (args != null)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var prop in args.GetType().GetProperties())
                {
                    sb.AppendFormat("{0}={1}&", prop.Name, prop.GetValue(args, null));
                }
                if (sb.Length > 0) sb.Remove(sb.Length - 1, 1);
                queryString = sb.ToString();
            }
            return queryString;
        }
    }
}

﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trello
{
    public static class RestClientExtensions
    {
        public static Task<IRestResponse> ExecuteTask(this IRestClient client, RestRequest request)
        {
            var TaskCompletionSource = new TaskCompletionSource<IRestResponse>();
            client.ExecuteAsync(request, (response, asyncHandle) =>
            {
                if (response.ResponseStatus == ResponseStatus.Error)
                    TaskCompletionSource.SetException(response.ErrorException);
                else
                    TaskCompletionSource.SetResult(response);
            });
            return TaskCompletionSource.Task;
        }

        public static Task<IRestResponse<T>> ExecuteAwaitableAsync<T>(this IRestClient client, IRestRequest request) where T : new()
        {
            var completionSource = new TaskCompletionSource<IRestResponse<T>>();
            client.ExecuteAsync<T>(request, (response, asyncHandle) =>
            {
                if (response.ErrorException != null)
                    completionSource.SetException(response.ErrorException);
                else
                    completionSource.SetResult(response);
            });
            return completionSource.Task;
        }
    }
}

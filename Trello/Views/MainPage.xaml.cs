﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Trello.Resources;
using RestSharp;
using RestSharp.Authenticators;

namespace Trello
{
    public partial class MainPage : PhoneApplicationPage
    {
        App app = (App.Current as App);
        public MainPage()
        {
            InitializeComponent();
            myBrowser.IsScriptEnabled = true;
        }

        private void myBrowser_NavigationFailed(object sender, NavigationFailedEventArgs e)
        {
            //TODO: Some exceptions behaviour
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            GetToken();
        }

        private void Navigated(object sender, NavigatingEventArgs e)
        {
            string returnURL = e.Uri.ToString();
            System.Diagnostics.Debug.WriteLine(returnURL);
            if (returnURL.Contains("http://127.0.0.1:6080/cb"))
            {
                e.Cancel = true;
                System.Diagnostics.Debug.WriteLine(returnURL);
                QueryString qs = new QueryString(returnURL);
                app.OAuthToken = qs["oauth_token"];
                app.Verifier = qs["oauth_verifier"];
                myBrowser.Visibility = System.Windows.Visibility.Collapsed;
                GetAccesToken();
            }
        }

        private async void GetToken()
        {

            app.Client.Authenticator = OAuth1Authenticator.ForRequestToken(app.ConsumerKey, app.ConsumerSecret);
            RestRequest request = new RestRequest("OAuthGetRequestToken", Method.POST);
            IRestResponse restResponse = await app.Client.ExecuteTask(request);
            HandleResponse(restResponse);
            OAuth();
        }

        private void HandleResponse(IRestResponse response)
        {
            var Response = response;
            System.Diagnostics.Debug.WriteLine(Response.Content);
            QueryString qs = new QueryString(Response.Content);
            app.OAuthToken = qs["oauth_token"];
            app.OAuthTokenSecret = qs["oauth_token_secret"];
            System.Diagnostics.Debug.WriteLine(app.OAuthToken);
            System.Diagnostics.Debug.WriteLine(app.OAuthTokenSecret);
        }


        private void OAuth()
        {
            var request = new RestRequest("OAuthAuthorizeToken", Method.GET);
            request.AddParameter("oauth_token", app.OAuthToken);
            request.AddParameter("oauth_token_secret", app.OAuthTokenSecret);
            request.AddParameter("expiration", "30days");
            request.AddParameter("scope", "read,write");
            request.AddParameter("name", "Trelloc!");
            request.AddParameter("return_url", "http://127.0.0.1:6080/cb");

            var url = app.Client.BuildUri(request);

            System.Diagnostics.Debug.WriteLine(url.ToString());

            myBrowser.Navigate(url);
        }


        private void GetAccesToken()
        {
            var request = new RestRequest("OAuthGetAccessToken", Method.POST);
            app.Client.Authenticator = OAuth1Authenticator.ForAccessToken(app.ConsumerKey, app.ConsumerSecret, app.OAuthToken, app.OAuthTokenSecret, app.Verifier);
            app.Client.ExecuteAsync(request, (response) =>
            {
                var Response = response;
                System.Diagnostics.Debug.WriteLine(Response.Content);
                QueryString qs = new QueryString(Response.Content);
                app.OAuthToken = qs["oauth_token"];
                app.OAuthTokenSecret = qs["oauth_token_secret"];
                System.Diagnostics.Debug.WriteLine(app.OAuthToken);
                System.Diagnostics.Debug.WriteLine(app.OAuthTokenSecret);
                NavigationService.Navigate(new Uri("/Views/BoardView.xaml", UriKind.Relative));
            });
        }

    }
}
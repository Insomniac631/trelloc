﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Trello.Models;
using Trello.ViewModels;

namespace Trello.Views
{
    public partial class BoardView : PhoneApplicationPage
    {

        public Test test;

        public BoardView()
        {
            InitializeComponent();
            test = new Test();
        }

        private void Test_Click(object sender, RoutedEventArgs e)
        {
            test.TestProc();
        }


    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trello.Models.Cards;

namespace Trello.ViewModels
{
    public class CardViewModel : ViewModelBase
    {
        public CardViewModel(Card card)
        {
            CardModel = card;
        }

        #region Properties

        public Card CardModel { get; set; }

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trello.Models;

namespace Trello.ViewModels
{
    public class Test
    {
        //some build-in tests, remove later
        public TrelloClient client;
        public string temp;

        public Test() 
        {
            client = TrelloClient.Instance;
        }

        public void TestProc()
        {
            System.Diagnostics.Debug.WriteLine("Id of MainMember: {0} ", client.MainMember.Id);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trello.ViewModels
{
    public abstract class TableViewModelBase<TItemType, TKeyType>
    {
        protected abstract ObservableCollection<TItemType> GetAllItems();

        protected abstract void DeleteItem(TKeyType id);

        protected abstract void GetItem(TKeyType id);

        protected abstract void InsertItem(TItemType item);

        //TODO: check if we need this
        protected abstract void UpdateItem(TKeyType id);
        
    }
}

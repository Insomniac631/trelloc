﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trello.Models.Boards;

namespace Trello.ViewModels
{
    public class BoardViewModel : ViewModelBase
    {

        public BoardViewModel(Board board) 
        {
            BoardModel = board;
        }


        #region Properties

        public Board BoardModel { get; set; }

        #endregion
    }
}
